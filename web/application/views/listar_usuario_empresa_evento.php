<div class="content-wrapper">  
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i>Lista de Eventos        
      </h1>
    </section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Listar Evento por Empresa</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>usuarioempresaeventoListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Id</th>
                      <th>Empresa</th>
                      <th>Evento</th>                     
                    </tr>
                    <?php
                    if(!empty($userEmpresaRecords))
                    {
                        
                        foreach($userEmpresaRecords as $record)
                        {
                    ?>
                    <tr>
                    <td><?php echo $record->id ?></td>
                    <td><?php echo $record->eventoId ?></td>
                    <td><?php echo $record->empresaId ?></td>
                    <td><?php echo $record->usersId ?></td>                   
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "usuarioempresaeventoListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
