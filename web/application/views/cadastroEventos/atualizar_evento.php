<?php
$this->load->view('template/header');
$this->load->view('template/menu');
?>
<!-- Main content -->
<div class="row">
    <div class="col-lg-12">
        <h1>Alteração de Evento</h1>
        <p>Altera Evento</p>
        <div class="col-lg-4">
            <div style="color: red">
                <?=validation_errors();?>
            </div>
            <?=form_open("cadastroEvento/atualizar_evento/$eventos->id")?>
                <div class="form-group">
                    <label for="nome">Nome:</label>
                    <!-- <input class="form-control" type="text" name="nome"> -->
                    <?=form_input([
                        'name' => 'nome',
                        'class' => 'form-control',
                        'value' => $this->input->post('nome') ?? $eventos->nome
                    ])?>
                </div>
                <div class="form-group">
                    <label for="local">Local:</label>
                    <input class="form-control" type="text" value="<?=$this->input->post('local') ?? $eventos->local?>" name="local">
                </div>              
                <div class="form-group">
                    <button class="btn btn-primary">Alterar Cliente</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>