﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Event.Util
{
    class ConnectionUtil
    {
        public static string URL_BASE = "http://7giros.com.br/aevent/WebService/";
        //public static string URL_BASE = "http://10.0.0.5/web/WebService/";
        //public static string URL_BASE = "http://172.22.16.254/web/WebService/";

        public static bool hasNetworkConnection()
        {
            var networkConnection = DependencyService.Get<INetworkConnection>();
            networkConnection.CheckNetworkConnection();
            return networkConnection.IsConnected;
        }

        public static async Task<string> performGetCallAsync(string partialUrl)
        {
            try
            {
                if (!hasNetworkConnection())
                {
                    return null;
                }

                var client = new HttpClient();
                client.BaseAddress = new Uri(URL_BASE);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string json = null;
                json = await client.GetAsync(URL_BASE + partialUrl).Result.Content.ReadAsStringAsync();
                return json;
            }
            catch (Exception e)
            {
                Debug.WriteLine("HTTP ERROR: " + e.Message);
                throw e;
            }
        }

        public static async Task<string> performPostCallAsync(string partialUrl, Dictionary<string, string> parameters)
        {
            try
            {
                if (!hasNetworkConnection())
                {
                    return null;
                }

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                FormUrlEncodedContent formContent = new FormUrlEncodedContent(parameters);

                var result = await client.PostAsync(URL_BASE + partialUrl, formContent);
                return await result.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                Debug.WriteLine("HTTP ERROR: " + e.Message);
                throw e;
            }
        }

        public static async Task<string> performDeleteCallAsync(string partialUrl)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string json = null;
                json = await client.DeleteAsync(URL_BASE + partialUrl).Result.Content.ReadAsStringAsync();
                return json;
            }
            catch (Exception e)
            {
                Debug.WriteLine("HTTP ERROR: " + e.Message);
                throw e;
            }
        }

    }
}
