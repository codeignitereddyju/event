<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Evento_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function eventoListarCount($searchText = '')
    {
        $this->db->select('BaseTbl.id,BaseTbl.nome, BaseTbl.local, tblev.id_palestra, tblev.nome_palestrante, tblev.nome_palestra, tblev.data, tblev.hora');
        $this->db->from('tbl_evento as BaseTbl');
        $this->db->join('tbl_palestra as tblev', 'tblev.fk_evento = BaseTbl.id','left');
        //'tbl_evento as ev', 'uee.eventoId = ev.id'
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.nome  LIKE '%".$searchText."%'
                            OR  BaseTbl.local  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
       
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function eventoListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.id,BaseTbl.nome, BaseTbl.local, tblev.id_palestra, tblev.nome_palestrante, tblev.nome_palestra, tblev.data, tblev.hora');
        $this->db->from('tbl_evento as BaseTbl');
        $this->db->join('tbl_palestra as tblev', 'tblev.fk_evento = BaseTbl.id','left');      
        if(!empty($searchText)) {
            $likeCriteria = "(  BaseTbl.nome  LIKE '%".$searchText."%'
                            OR  BaseTbl.local  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }          
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    /**
     * This function is used to get the full list of events
     * @return array $result : This is result
     */
    function eventoListingAll()
    {
        $this->db->select('*');
        $this->db->from('tbl_evento');       
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getEventoRoles()
    {
        $this->db->select('roleId, role');
        $this->db->from('tbl_roles');
        $this->db->where('roleId !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNovoEvento($eventoInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_evento', $eventoInfo);        
        $insert_id = $this->db->insert_id();        
        $this->db->trans_complete();
        
        return $insert_id;

    }
    
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getEventoInfo($id, $palestra)
    {
        $this->db->select('id, nome, local, data, hora, descricao');
        $this->db->from('tbl_evento'); 
        $this->db->join('tbl_palestra as palestra','palestra.id = tbl_evento.id');       		
        $this->db->where('id', $id, 'id_palestra', $palestra);
        $query = $this->db->get();
        
        return $query->result();
    }    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editEvento($eventoInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_evento', $eventoInfo);
        
        return TRUE;
    }
       
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteEvento($id, $eventoInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_evento', $eventoInfo);
        
        return $this->db->affected_rows();
    }

    
    function Formatar($evento){
        if($evento){
          for($i = 0; $i < count($evento); $i++){
            $evento[$i]['editar_url'] = base_url('editar')."/".$evento[$i]['id'];
            $evento[$i]['excluir_url'] = base_url('excluir')."/".$evento[$i]['id'];
          }
          return $evento;
        } else {
          return false;
        }
      }
}

  