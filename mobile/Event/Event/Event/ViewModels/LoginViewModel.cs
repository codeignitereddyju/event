﻿using Event.Util;
using Event.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Event.ViewModels
{
    class LoginViewModel : BaseViewModel
    {
        public LoginViewModel()
        {
            username = "admin@bewithdhanu.in";
            password = "123456";

            OnLoginButtonClicked = new Command(async () => await OnLoginButtonAsync());
            OnRegisterButtonClicked = new Command(async () => await OnRegisterButtonAsync());
        }

        public async Task OnLoginButtonAsync()
        {
            if (username.Trim() != null && password.Trim() != null)
            {
                try
                {
                    Dictionary<string, string> parameter = new Dictionary<string, string>();
                    parameter.Add("email", username.Trim());
                    parameter.Add("password", password.Trim());

                    string result = await ConnectionUtil.performPostCallAsync("loginMobile", parameter);
                    
                    if (result.Contains("userId"))
                    {
                        Application.Current.MainPage = new MainPage();
                    }
                    else
                    {
                        await DisplayAlert("Erro", "Login ou senha incorretos");
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                    await DisplayAlert("Erro", e.ToString());
                }
            }
        }

        public async Task OnRegisterButtonAsync()
        {
            Application.Current.MainPage = new RegisterPage();
        }

        public async Task DisplayAlert(string title, string message)
        {
            await Application.Current.MainPage.DisplayAlert(title, message, "Ok");
        }
        private string _username;
        public string username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }
        private string _password;
        public string password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }
        public ICommand OnLoginButtonClicked { get; private set; }
        public ICommand OnRegisterButtonClicked { get; private set; }
    }
}
