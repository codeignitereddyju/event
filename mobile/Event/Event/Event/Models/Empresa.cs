﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Event.Models
{
    public class Empresa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
    }
}
