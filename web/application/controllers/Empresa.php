<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';


class Empresa extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('empresa_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Empresa';
        
        $this->loadViews("listar_empresa", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the user list
     */
    function empresaListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('empresa_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->empresa_model->empresaListarCount($searchText);

			$returns = $this->paginationCompress ( "empresaListing/",$count,5);
            
            $data['empresaRecords'] = $this->empresa_model->empresaListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = ' Listar Empresa';
            
            $this->loadViews("listar_empresa", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
   
    function addEmpresa()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('empresa_model');
            $data = $this->empresa_model->getempresaRoles();
            
            $this->global['pageTitle'] = 'Adicionar nova empresa empresa';

            $this->loadViews("novoEmpresa", $this->global, $data, NULL);
        }
    }
      
    /**
     * This function is used to add new user to the system
     */
    
    function addNovaEmpresa()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }

        else
        {
            $this->load->library('form_validation');            
            $this->form_validation->set_rules('nome','Nome','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('cnpj','CNPJ','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('cidade','Cidade','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('estado','Estado','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('cep','CEP','trim|required|max_length[128]|xss_clean');       
            $this->form_validation->set_rules('endereco','endereco','trim|required|max_length[128]|xss_clean'); 
            $this->form_validation->set_rules('razaosocial','Razaosocial','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean');
                       
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addEmpresa();
            }
            else
            {                
                $nome = ucwords(strtolower($this->input->post('nome')));                
                $cnpj = $this->input->post('cnpj');
                $cidade = $this->input->post('cidade');
                $estado = $this->input->post('estado');
                $email = $this->input->post('email');
                $cep = $this->input->post('cep');
                $endereco = $this->input->post('endereco');
                $razaosocial = $this->input->post('razaosocial');
                $role = $this->input->post('role');
                $mobile = $this->input->post('mobile');

                $empresaInfo = array('nome'=> $nome, 'cnpj'=>$cnpj, 'cidade'=>$cidade, 'estado'=>$estado, 'email'=>$email, 'cep'=>$cep, 'endereco'=>$endereco, 'razaosocial'=>$razaosocial, 'role'=>$role, 'mobile'=>$mobile);
                $this->load->model('empresa_model');
                $result = $this->empresa_model->addNovaEmpresa($empresaInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'Novo empresa criado com sucesso');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Erro ao criar empresa');
                }
                
                redirect('addNovaEmpresa');
            }
        }
    }
    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    /*
    function editarempresa($id = NULL)
    {
        if($this->isAdmin() == TRUE || $id == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($id == null)
            {
                redirect('empresaListing');
            }            
            
            $data['empresaInfo'] = $this->empresa_model->getempresaInfo($id);
            
            $this->global['pageTitle'] = 'CodeInsect : Editar empresa';
            
            $this->loadViews("editempresa", $this->global, $data, NULL);
        }
    }
    */
    
    /**
     * This function is used to edit the user information
     */
    /*
    function editempresa()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
            
            $this->form_validation->set_rules('nome','Nome','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('local','Local','trim|required|max_length[128]|xss_clean');          
                       
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editempresa($id);
            }
            else
            {
                $nome = ucwords(strtolower($this->input->post('name')));
                $local = $this->input->post('local');               
                                
                $empresaInfo = array();
                
                if(empty($password))
                {
                    $empresaInfo = array('nome'=>$nome, 'local'=>$local);
                }
                else
                {
                    $empresaInfo = array('nome'=>ucwords($nome));
                }
                
                $result = $this->empresa_model->editUser($empresaInfo, $id);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'empresa cadastrado com sucesso');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Ao Cadastrar empresa ocorreu um erro');
                }
                
                redirect('empresaListing');
            }
        }
    }
*/

    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    /*
    function deleteempresa()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $id = $this->input->post('id');
            $empresaInfo = array('isDeleted'=>1);
            
            $result = $this->empresa_model->deleteempresa($id, $empresaInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
         */ 
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>