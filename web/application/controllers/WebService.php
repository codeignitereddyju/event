<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : WebService
 * WebService class to control mobile events.
 * @author : Edileuson
 * @version : 1.0
 * @since : 20 Janeiro 2019
 */
class WebService extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('evento_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->load->view('login');
    }

    /**
     * Login no sitema
     */
    function loginMobile()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        // $email = 'admin@bewithdhanu.in';
        // $password = '123456';
        
        $result = $this->login_model->loginMe($email, $password);
        if(count($result) > 0)
        {
            foreach ($result as $res)
            {
                $sessionArray = array('userId'=>$res->userId, 'role'=>$res->roleId, 'roleText'=>$res->role,
                    'name'=>$res->name, 'isLoggedIn' => TRUE );
                echo json_encode($res);
            }
        }
    }

    /**
     * Criar usuario
     */
    function createUser()
    {
        $name = ucwords(strtolower($this->input->post('name')));
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $roleId = $this->input->post('role');//always 3
        $eventId = $this->input->post('event');
        $mobile = $this->input->post('mobile');
        
        $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId, 'eventId'=> $eventId, 'name'=> $name,
                            'mobile'=>$mobile, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
        
        $this->load->model('user_model');
        $result = $this->user_model->addNewUser($userInfo);

        echo $return;
    }

    /**
     * Listar todos eventos
     */
    function listarEventos()
    {
        $this->load->model('evento_model');
        $result = $this->evento_model->eventoListingAll();
        echo json_encode($result);
    }

    /**
     * Listar todas empresas
     */
    function listarEmpresas()
    {
        $this->load->model('empresa_model');
        $result = $this->empresa_model->empresaListingAll();
        echo json_encode($result);
    }

    /**
     * Listar todas empresas visitadas
     */
    function listarEmpresasVisitadas()
    {
        $userid = $this->input->post('userid');
        $eventoid = $this->input->post('eventoid');
        $this->load->model('empresa_model');
        $result = $this->empresa_model->getempresavisitadaevento($userid, $eventoid);
        echo json_encode($result);
    }

    /**
     * Listar todas empresas
     */
    function listarUsersEmpresasEventos()
    {
        $this->load->model('user_empresa_evento_model');
        $result = $this->user_empresa_evento_model->userEmpresaEventoListingWithName();
        echo json_encode($result);
    }
}

?>