﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Event.Views
{
    public class MainPageMenuItem
    {
        public MainPageMenuItem()
        {
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Img { get; set; }
        public Type TargetType { get; set; }
    }
}
