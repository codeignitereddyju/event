﻿using Event.Models;
using Event.Util;
using Event.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Event.ViewModels
{
    public class RegisterViewModel : BaseViewModel
    {
        public ObservableCollection<Evento> Eventos { get; set; }
        
        public RegisterViewModel()
        {
            Eventos = new ObservableCollection<Evento>();
            ExecuteLoadItemsCommandAsync();
            OnSaveButtonClicked = new Command(async () => await OnSaveButtonAsync());
        }

        async void ExecuteLoadItemsCommandAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                string result = await ConnectionUtil.performGetCallAsync("listarEventos");

                List<Evento> eventos = JsonConvert.DeserializeObject<List<Evento>>(result);
                foreach (var evento in eventos)
                {
                    Eventos.Add(evento);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task OnSaveButtonAsync()
        {
            if (!password.Equals(confirmPassword))
            {
                await DisplayAlert("Atenção!", "As senhas informadas não são iguais, tente novamente!");
                return;
            }
            try
            {
                Dictionary<string, string> parameter = new Dictionary<string, string>();
                parameter.Add("name", name.Trim());
                parameter.Add("email", email.Trim());
                parameter.Add("password", password.Trim());
                parameter.Add("event", Evento.Id.ToString());
                parameter.Add("mobile", mobile.Trim());

                string result = await ConnectionUtil.performPostCallAsync("createUser", parameter);

                if (result != "")
                {
                    await DisplayAlert("Sucesso!", "Cadastro realizado com sucesso!");
                    Application.Current.MainPage = new LoginPage();
                }
                else
                {
                    await DisplayAlert("Erro!", "Algo deu errado, tente novamente!");
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                await DisplayAlert("Erro", e.ToString());
            }
            
        }

        private Evento _evento;
        private Evento Evento
        {
            get { return _evento; }
            set
            {
                SetProperty(ref _evento, value);
            }
        }
        private string _name;
        public string name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
            }
        }
        private string _email;
        public string email
        {
            get { return _email; }
            set
            {
                SetProperty(ref _email, value);
            }
        }
        private string _mobile;
        public string mobile
        {
            get { return _mobile; }
            set
            {
                SetProperty(ref _mobile, value);
            }
        }
        private string _password;
        public string password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }
        private string _confirmPassword;
        public string confirmPassword
        {
            get { return _confirmPassword; }
            set { SetProperty(ref _confirmPassword, value); }
        }

        private int _eventosSelectedIndex;
        public int EventosSelectedIndex
        {
            get { return _eventosSelectedIndex; }
            set
            {
                SetProperty(ref _eventosSelectedIndex, value);
                //if (_countriesSelectedIndex != value)
                //{
                //    _countriesSelectedIndex = value;

                //    // trigger some action to take such as updating other labels or fields
                //    OnPropertyChanged(nameof(CountriesSelectedIndex));
                //    SelectedCountry = Countries[_countriesSelectedIndex];
                //}
            }
        }

        public async Task DisplayAlert(string title, string message)
        {
            await Application.Current.MainPage.DisplayAlert(title, message, "Ok");
        }

        public ICommand OnSaveButtonClicked { get; private set; }
    }
}
