<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Empresa_model extends CI_Model
{

    function empresaListarCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.nome, BaseTbl.cnpj, BaseTbl.cidade, BaseTbl.estado, BaseTbl.email,
        BaseTbl.cep, BaseTbl.endereco, BaseTbl.razaosocial');
        $this->db->from('tbl_empresa as BaseTbl');
        //$this->db->join('tbl_roles as Role','Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(
                            BaseTbl.nome LIKE '%".$searchText."%'
                            OR BaseTbl.cnpj LIKE '%".$searchText."%'
                            OR BaseTbl.cidade LIKE '%".$searchText."%'
                            OR BaseTbl.estado LIKE '%".$searchText."%'
                            OR BaseTbl.email LIKE '%".$searchText."%'
                            OR BaseTbl.cep LIKE %".$searchText."%'
                            OR BaseTbl.endereco LIKE %".$searchText."%'
                            OR BaseTbl.razaosocial LIKE %".$searchText."%')";
            $this->db->where($likeCriteria);
        }        
        //$this->db->where('BaseTbl.roleId !=', 1);

        $query = $this->db->get();        
        return count($query->result());
    }

    function empresaListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.id, BaseTbl.nome, BaseTbl.cnpj, BaseTbl.cidade, BaseTbl.estado, BaseTbl.email,
        BaseTbl.cep, BaseTbl.endereco, BaseTbl.razaosocial');
        $this->db->from('tbl_empresa as BaseTbl');
        //$this->db->join('tbl_roles as Role','Role.roleId = BaseTbl.roleId','left');
    
        if(!empty($searchText)) {
            $likeCriteria = "(  
                BaseTbl.nome LIKE '%".$searchText."%'
            OR BaseTbl.cnpj LIKE '%".$searchText."%'
            OR BaseTbl.cidade LIKE '%".$searchText."%'
            OR BaseTbl.estado LIKE '%".$searchText."%'
            OR BaseTbl.email LIKE '%".$searchText."%'
            OR BaseTbl.cep LIKE %".$searchText."%'
            OR BaseTbl.endereco LIKE %".$searchText."%'
            OR BaseTbl.razaosocial LIKE %".$searchText."%'
            )";

            $this->db->where($likeCriteria);
        }        
        
        
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    function addNovaEmpresa($empresaInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_empresa', $empresaInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
     
    /**
     * This function is used to get the full list of empresas
     * @return array $result : This is result
     */
    function empresaListingAll()
    {
        $this->db->select('*');
        $this->db->from('tbl_empresa');       
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }  

    function getEmpresaRoles()
    {
        $this->db->select('roleId, role');
        $this->db->from('tbl_roles');
        $this->db->where('roleId !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    function getEmpresaInfo($id)
    {
        $this->db->select('id, nome, cnpj, cidade, estado, email, cep, endereco, razaosocial, role, mobile');
        $this->db->from('tbl_empresa');
        $this->db->where('isDeleted', 0);		
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->result();
    }    

    function getempresavisitadaevento($userid, $eventoid)
    {
        $this->db->select('tbl_empresa.id, tbl_empresa.nome, tbl_empresa.cidade, tbl_empresa.estado, tbl_users_evento_empresa.id as checked');

        $this->db->from('tbl_empresa');

        $this->db->join('tbl_users_evento_empresa','tbl_empresa.id = tbl_users_evento_empresa.empresaId','left');
        
        if(!empty($userid)) {
            $likeCriteria = "(                  
                    tbl_users_evento_empresa.usuarioId = ".$userid."
                    AND tbl_users_evento_empresa.eventoId LIKE ".$eventoid."
                )";
                $this->db->where($likeCriteria);
        }
       
        $query = $this->db->get();
        
        $result = $query->result();

        return $result;
    } 
  
}  
	
