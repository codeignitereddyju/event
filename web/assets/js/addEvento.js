/**
 * File : addUser.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addEventoForm = $("#addEvento");
	
	var validator = addEventoForm.validate({
		
		rules:{
			nome :{ required : true },			
			local : { required : true },			
			
		},
		messages:{
			name :{ required : "Nome do evento está vázio" },			
			local : { required : "Local do evento está vázio" },		
					
		}
	});
});
