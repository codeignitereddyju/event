﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Event.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPageMaster : ContentPage
	{
        public ListView ListView;
        //public Label Name;

        public MainPageMaster()
        {
            InitializeComponent();

            BindingContext = new MainPageMasterViewModel();
            ListView = MenuItemsListView;
        }

        class MainPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MainPageMenuItem> MenuItems { get; set; }

            public MainPageMasterViewModel()
            {
                MenuItems = new ObservableCollection<MainPageMenuItem>(new[]
                {
                    new MainPageMenuItem { Id = 0, Title = "Home",TargetType = typeof(MainPageDetail), Img = "home_24dp.png"},
                    new MainPageMenuItem { Id = 1, Title = "Responsável",TargetType = typeof(LecturePage) ,Img = "account_24dp.png"},
                    new MainPageMenuItem { Id = 2, Title = "Animal",TargetType = typeof(LecturePage),Img = "pet_24dp.png" },
                    new MainPageMenuItem { Id = 3, Title = "Agenda",TargetType = typeof(LecturePage),Img = "date_24dp.png" },
                    new MainPageMenuItem { Id = 3, Title = "Relatório",TargetType = typeof(LecturePage),Img = "description_24dp.png" },
                    new MainPageMenuItem { Id = 4, Title = "Mudar Senha",TargetType = typeof(AboutPage),Img = "senha_24dp.png" },
                    new MainPageMenuItem { Id = 4, Title = "Sair",TargetType = typeof(LoginPage),Img = "exit_24dp.png" }
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}