<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Novo Evento
      
      </h1>
    </section>    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Detalhes do Evento</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="addEvento" action="<?php echo base_url() ?>addNovoEvento" method="post" role="form">
                        <div class="box-body">                            
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="nome">Nome</label>
                                        <input type="text" class="form-control required" id="nome" name="nome" maxlength="128">
                                    </div>
                                </div>
                            </div>                          
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="local">Local</label>
                                        <input type="text" class="form-control required" id="local" name="local" maxlength="500">
                                    </div>
                                </div>                               
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="palestrante">Palestrante</label>
                                        <input type="text" class="form-control required" id="palestrante" name="palestrante" maxlength="500">
                                    </div>
                                </div>                               
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nPalestra">Nome Palestra</label>
                                        <input type="text" class="form-control required" id="nPalestra" name="nPalestra" maxlength="500">
                                    </div>
                                </div>                               
                            </div>                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="descricao">Descrição</label>
                                        <input type="text" class="form-control required" id="descricao" name="descricao" maxlength="500">
                                    </div>
                                </div>                               
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="data">Data</label>
                                        <input type="date" class="form-control required" id="data" name="data">
                                    </div>
                                </div>                               
                            </div>  
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="data">Hora</label>
                                        <input type="text" class="form-control required" id="hora" name="hora">
                                    </div>
                                </div>                               
                            </div>                                  
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addEvento.js" type="text/javascript"></script>