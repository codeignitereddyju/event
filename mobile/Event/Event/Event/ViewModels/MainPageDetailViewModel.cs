﻿using Event.Models;
using Event.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Event.ViewModels
{
    public class MainPageDetailViewModel : BaseViewModel
    {
        public ObservableCollection<Empresa> Empresas { get; set; }
        public Command LoadEmpresasCommand { get; set; }

        public MainPageDetailViewModel()
        {
            Empresas = new ObservableCollection<Empresa>();
            LoadEmpresasCommand = new Command(async () => await ExecuteLoadEmpresasCommandAsync());
        }

        async Task ExecuteLoadEmpresasCommandAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                Empresas.Clear();
                string result = await ConnectionUtil.performGetCallAsync("listarEmpresas");

                List<Empresa> empresas = JsonConvert.DeserializeObject<List<Empresa>>(result);
                foreach (var empresa in empresas)
                {
                    Empresas.Add(empresa);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
